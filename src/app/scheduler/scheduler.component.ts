import { Component, OnInit, AfterViewInit } from '@angular/core';
import { StripeCheckoutLoader, StripeCheckoutHandler } from 'ng-stripe-checkout';
import { QuestionnaireService } from '../services/_questionnaire/questionnaire.service';
import { HttphelperService } from '../services/_http/httphelper.service';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit, AfterViewInit {

  private stripeCheckoutHandler: StripeCheckoutHandler;

  constructor(
    private stripeCheckoutLoader: StripeCheckoutLoader,
    private questionnaire: QuestionnaireService,
    private http: HttphelperService
  ) { }

  public ngAfterViewInit() {
    this.stripeCheckoutLoader.createHandler({
      key: 'pk_test_Ous41lDL0LRLIHSk6athVKfK00fpxfE1J7',
      image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
      token: (token) => {
        // Do something with the token... <- send token to backend here

        console.log('Payment successful!', token);
      },
    }).then((handler: StripeCheckoutHandler) => {
      this.stripeCheckoutHandler = handler;
    });
  }

  public onClickBuy() {
    this.stripeCheckoutHandler.open({
      amount: (this.questionnaire.total) * 100,
      currency: 'usd',
    }).then((token) => {
      // Do something with the token...
      this.http.processStripe(token);
      console.log('Payment successful!', token);
    }).catch((err) => {
      // Payment failed or was canceled by user...
      if (err !== 'stripe_closed') {
        throw err;
      }
    });
  }

  public onClickCancel() {
    // If the window has been opened, this is how you can close it:
    this.stripeCheckoutHandler.close();
  }

  ngOnInit() {
    const exerciseReportContainer = document.getElementById('exerciseWrapper');
    exerciseReportContainer.setAttribute('style', 'height: 715px;');

  }
}
