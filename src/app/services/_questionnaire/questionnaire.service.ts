import { Injectable } from '@angular/core';
import { HttphelperService } from '../_http/httphelper.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class QuestionnaireService {

  data$: Observable<any>;
  title: string;
  questions: any;
  subtotal: number;
  total: number;
  items = [];

  constructor(private http: HttphelperService) { }

  getData() {
    this.data$ = this.http.getQuestions();
    this.data$.subscribe(res => {

      this.title = res.catagoryTitle;
      this.questions = res.questions;
      this.subtotal = res.catagoryBasePrice;

      const id = res.catagoryBaseService;
      const price = res.catagoryBasePrice;
      this.items.push({ id, price });
    });
    console.log(this.items);
  }

  clear() {
    this.items = [];
    this.total = 0;
    this.subtotal = 0;
  }
}
