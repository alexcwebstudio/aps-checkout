import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SchedulerService {

  hasStarted = false;
  hasFinished = false;

  constructor() { }
}
