import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttphelperService {

  readonly processLocation = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  getQuestions() {
    return this.http.get('../../../assets/data/openings.json');
  }

  processStripe(token) {
    this.http.post('http://127.0.0.1:3000/testing', token).subscribe();
  }

}
