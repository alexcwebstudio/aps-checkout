import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { QuestionviewComponent } from './questionview/questionview.component';

const routes: Routes = [
  { path: '', component: LandingpageComponent },
  { path: 'questions', component: QuestionviewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
