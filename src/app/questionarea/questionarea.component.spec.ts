import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionareaComponent } from './questionarea.component';

describe('QuestionareaComponent', () => {
  let component: QuestionareaComponent;
  let fixture: ComponentFixture<QuestionareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
