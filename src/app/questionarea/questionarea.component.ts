import { Component, OnInit } from '@angular/core';
import { QuestionnaireService } from '../services/_questionnaire/questionnaire.service';
import { SchedulerService } from '../services/_scheduler/scheduler.service';

@Component({
  selector: 'app-questionarea',
  templateUrl: './questionarea.component.html',
  styleUrls: ['./questionarea.component.scss']
})
export class QuestionareaComponent implements OnInit {

  constructor(public questionnaire: QuestionnaireService, public scheduler: SchedulerService) { }

  hasStarted = false;
  hasFinished = false;
  index = 0;


  parseSelection(id: string, price: number) {
    const jsonData = { id, price };
    return jsonData;
  }

  hasItem(id: string, price: number) {
    this.questionnaire.subtotal += price;
    this.questionnaire.items.push(this.parseSelection(id, price));
    this.updateIndex();
  }

  updateIndex() {
    if (this.index < this.questionnaire.questions.length - 1) {
      this.index++;
    } else {
      this.hasFinished = true;
      this.questionnaire.total = this.questionnaire.subtotal + (this.questionnaire.subtotal * .06);
    }
  }

  ngOnInit() {
    this.questionnaire.clear();
    this.questionnaire.getData();
  }
}
