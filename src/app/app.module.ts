import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StripeCheckoutModule } from 'ng-stripe-checkout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeviewComponent } from './homeview/homeview.component';
import { HttpClientModule } from '@angular/common/http';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { NavComponent } from './nav/nav.component';
import { QuestionviewComponent } from './questionview/questionview.component';
import { QuestionareaComponent } from './questionarea/questionarea.component';
import { SchedulerComponent } from './scheduler/scheduler.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeviewComponent,
    LandingpageComponent,
    NavComponent,
    QuestionviewComponent,
    QuestionareaComponent,
    SchedulerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StripeCheckoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
